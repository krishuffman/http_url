package httpUrl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Main {

        private static final String line = System.getProperty("line.separator");//make things more readable
        public static void main(String s1[]) {
            try {
                URL url = new URL("https://www.krishuffman.com/");//try connecting to my website
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                String readStream = readStream(con.getInputStream());
                System.out.println(readStream);
                con.setConnectTimeout(5000);//timeout after 5 secs
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private static String readStream(InputStream in) {
            StringBuilder sb = new StringBuilder();
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(in));) {
                String nextLine = "";
                while ((nextLine = reader.readLine()) != null) {
                    sb.append(nextLine +line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return sb.toString();//display data
        }
    }
